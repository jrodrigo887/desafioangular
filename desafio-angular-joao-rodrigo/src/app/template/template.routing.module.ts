import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListarHistoricoComponent } from '../feature/components/listar-historicos/listar-historico.component';
import { PrecoBaseMunicipioComponent } from '../feature/components/preco-base-municipio/preco-base-municipio.component';

const routes: Routes = [
  {
    path: 'historico',
    component: ListarHistoricoComponent,
    //children: [
      //{ path: 'cadastrar/:id',
      //loadChildren: () => import('../feature/components/cadastrar-historico/cadastrar-historico.module')
        //.then(m => m.CadastrarHistoricoModule) },
    //]
  },
  { path: 'combustivel-info',
    component: PrecoBaseMunicipioComponent,
      loadChildren: () =>
      import('../feature/components/preco-base-municipio/base-municipio.module')
        .then(m => m.BaseMunicipioModule)
      },


  //{ path: '', redirectTo: 'historico', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [
    RouterModule,
  ],

})
export class TemplateRoutingModule { }
