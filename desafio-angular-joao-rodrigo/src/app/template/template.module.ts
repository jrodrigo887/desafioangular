import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HeaderComponent } from './header/header.component';
import { NavComponent } from './nav/nav.component';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatMenuModule} from '@angular/material/menu';
import {MatIconModule} from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import {MatTableModule} from '@angular/material/table';
import { TemplateComponent } from './template.component';
import { TemplateRoutingModule } from './template.routing.module';
import { HttpClientModule } from '@angular/common/http';
import { MatSortModule } from '@angular/material/sort';
import { FormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import { CadastrarHistoricoComponent } from '../feature/components/cadastrar-historico/cadastrar-historico.component';
import { BaseMunicipioModule } from '../feature/components/preco-base-municipio/base-municipio.module';
import { CadastrarHistoricoModule } from '../feature/components/cadastrar-historico/cadastrar-historico.module';
import { HistoricoModule } from '../feature/components/listar-historicos/historico.module';



@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    TemplateRoutingModule,
    MatToolbarModule,
    MatSidenavModule,
    MatMenuModule,
    MatIconModule,
    MatListModule,
    MatTableModule,
    MatSortModule,
    FormsModule,
    MatDialogModule,
    BaseMunicipioModule,
    CadastrarHistoricoModule,
    HistoricoModule,
  ],
  declarations: [
    TemplateComponent,
    HeaderComponent,
    NavComponent,

  ],
  exports: [
    TemplateComponent,
    CommonModule,

  ],

  entryComponents: [
    CadastrarHistoricoComponent,
  ],



})
export class TemplateModule { }
