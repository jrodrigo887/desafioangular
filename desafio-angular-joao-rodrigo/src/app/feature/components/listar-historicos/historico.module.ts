import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListarHistoricoComponent } from './listar-historico.component';
import { MatTableModule } from '@angular/material/table';



@NgModule({
  declarations: [
    ListarHistoricoComponent,
  ],
  imports: [
    CommonModule,
    //MatFormFieldModule,
    //MatInputModule,
    //ReactiveFormsModule,
    //MatDialogModule,
    MatTableModule,
  ]
})
export class HistoricoModule { }
