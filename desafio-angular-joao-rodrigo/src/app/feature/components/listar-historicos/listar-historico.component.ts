import { Component, OnInit } from '@angular/core';
import { HistoricoControle } from 'src/app/core/interfaces/historicoControle.modelo.interface';
import { ConvertObject } from 'src/app/core/utils/convertObjectToArray';
import { HistoricoService } from '../service/historico.service';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { CadastrarHistoricoComponent } from '../cadastrar-historico/cadastrar-historico.component';
import { MetodosHttp } from 'src/app/core/enums/metodos.http';
import { Subject, pipe } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { BasePrecoMunicipioService } from '../service/base-preco-municipio.service';

@Component({
  selector: 'app-listar-historico',
  templateUrl: './listar-historico.component.html',
  styleUrls: ['./listar-historico.component.css']
})
export class ListarHistoricoComponent implements OnInit {

  unsubscribe$ = new Subject();
  displayedColumns: string[];
  dataSource = [];
  dialogRef: MatDialogRef<CadastrarHistoricoComponent>;

  constructor(
    private historicoService: HistoricoService,
    private baseMuncService: BasePrecoMunicipioService,
    private dialog: MatDialog,
  ) {}

   ngOnInit(): void {
    this.getHistoricos();
  }


  private getHistoricos() {
    this.historicoService.getHistorico()
    .subscribe(
      (vl: HistoricoControle[]) => {
        this.displayedColumns = new ConvertObject(vl[0]).toArray;
        this.dataSource = vl
      }
    );
  }

  novoHistorico(): void {
    this.dialogRef = this.openDialog(null);
    this.dialogRef.afterClosed()
      .subscribe(value => {
        this.save(value, MetodosHttp.POST)
        },
    )
  }

  public onUpdate(historico: HistoricoControle) {
    this.dialogRef = this.openDialog(historico);
    this.dialogRef.afterClosed()
    .subscribe(
      value =>
      this.save(value, MetodosHttp.PUT),
    )
  }

  private save(value, metodo): any {
    return this.historicoService
      .saveHistoricoPostOrPut(value, metodo)
      .pipe(
        takeUntil(this.unsubscribe$),

      )
      .subscribe(
        () => alert('Dados Salvo.'),
        () => alert('Erros ao salvar.'),
        () => this.ngOnInit()
      );
  }

  public onDelete(id: number) {
    this.historicoService
    .deleteHistorico(id)
      .subscribe(
      () => {
        alert('Deletado com Sucesso.');
      },
      (err) => alert('Erro ao deletar.'),
      () => this.ngOnInit()
    );
  }

  private openDialog(data): MatDialogRef<CadastrarHistoricoComponent> {
    return this.dialog.open(CadastrarHistoricoComponent, {
      width: '300px',
      data,
    });
  }


  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }


}
