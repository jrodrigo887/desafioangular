import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PrecoBaseMunicipioComponent } from './preco-base-municipio.component';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { MatTableModule } from '@angular/material/table';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    //HttpClientModule,
   // MatToolbarModule,
    //MatSidenavModule,
    MatMenuModule,
    MatIconModule,
   // MatListModule,
    MatTableModule,
    //FormsModule,
  ],
  declarations: [
    PrecoBaseMunicipioComponent
  ]
})
export class BaseMunicipioModule { }
