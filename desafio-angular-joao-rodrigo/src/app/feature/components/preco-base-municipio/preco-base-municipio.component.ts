import { Component, OnInit, OnDestroy } from '@angular/core';
import { of, Subject } from 'rxjs';
import { delay, isEmpty, map, mergeMap, switchMap, takeUntil, tap } from 'rxjs/operators';
import { Municipio } from 'src/app/core/interfaces/municipioInterface';
import { BasePrecoMunicipioService } from '../service/base-preco-municipio.service';

@Component({
  selector: 'app-preco-base-municipio',
  templateUrl: './preco-base-municipio.component.html',
  styleUrls: ['./preco-base-municipio.component.css']
})
export class PrecoBaseMunicipioComponent implements OnInit, OnDestroy {

  unsubscribe$ = new Subject<void>();
  displayedColumns;
  dataSource = [];
  metadados= [];

  constructor(
    private baseMunicipio: BasePrecoMunicipioService) { }



  ngOnInit(): void {
    this.displayedColumns = ['municipio', 'media'];

    this.baseMunicipio.getMunicipios()
    .pipe(
      tap(),
      switchMap((vl) => this.getCidades(vl) ),
      map(obs => console.log(obs) )
    ).subscribe();

  }


  private getCidades = (params: Municipio[]) => {
    return of(params)
    .pipe(
      map(r => r.map((res) => {
        return this.baseMunicipio
            .getMediaPrecoMunicipio(res.atributo)
            .pipe(
              map(values => {
                return {
                  media: values
                }
              })
            )
      }) )
    )
  }

  mediaCidade(v) {
    let media;
    this.baseMunicipio.getMediaPrecoMunicipio(v)
    .subscribe(
      md => {
        media = Number(md).toFixed(2);
      }
    )
    return media
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

}
