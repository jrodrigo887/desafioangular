import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrecoBaseMunicipioComponent } from './preco-base-municipio.component';

describe('PrecoBaseMunicipioComponent', () => {
  let component: PrecoBaseMunicipioComponent;
  let fixture: ComponentFixture<PrecoBaseMunicipioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PrecoBaseMunicipioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PrecoBaseMunicipioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
