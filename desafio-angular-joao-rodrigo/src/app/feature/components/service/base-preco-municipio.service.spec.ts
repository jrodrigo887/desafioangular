/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { BasePrecoMunicipioService } from './base-preco-municipio.service';

describe('Service: BasePrecoMunicipio', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BasePrecoMunicipioService]
    });
  });

  it('should ...', inject([BasePrecoMunicipioService], (service: BasePrecoMunicipioService) => {
    expect(service).toBeTruthy();
  }));
});
