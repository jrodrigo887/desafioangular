import { HttpClient } from '@angular/common/http';
import { EventEmitter, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { take, tap } from 'rxjs/operators';
import { MetodosHttp } from 'src/app/core/enums/metodos.http';
import { Usuario } from 'src/app/core/interfaces/usuario.model.interface';
import { environment } from 'src/environments/environment';

const api = `${environment.api_combustivel}/usuarios`;

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  constructor(
    private http: HttpClient
  ) {}

  public getUsuarios(): Observable<Usuario[]> {
    return this.http.get<Usuario[]>(api);
  }

  public getUsuariosById(idUsuario: number): Observable<Usuario> {
    return this.http.get<Usuario>(`${api}/${idUsuario}`);
  }

  public saveUsuarioPostOrPut(dados: Usuario, metodo: MetodosHttp): Observable<any> {
    if (metodo === MetodosHttp.POST) {
      return this.http.post(api, dados);
    } else {
      return this.http.put(api, dados);
    }
  }

  public deleteUsuario(idUsuario: number): Observable<any> {
    return this.http.delete(`${api}/${idUsuario}`);
  }






}

export const usuariosMock: Usuario[] = [
  {
    "id": 393,
    "nome": "Taysa",
    "email": "taysa@gmail.com",
    "login": "taysa",
    "admin": false
    },
    {
    "id": 402,
    "nome": "Rodrigo",
    "email": "rodrigol@rodrigol.com",
    "login": "rodrigol",
    "admin": false
    },
    {
    "id": 406,
    "nome": "Rodolfo",
    "email": "rodolfo@rodolfo.com",
    "login": "Rodolfo",
    "admin": false
    },
    {
    "id": 411,
    "nome": "sivirino",
    "email": "teste@gmail.com",
    "login": "teste1",
    "admin": false
    },
    {
    "id": 379,
    "nome": "Diogo",
    "email": "diogo@teste.com",
    "login": "DiogoBarbosa",
    "admin": false
    },
    {
    "id": 332,
    "nome": "Rodrigo Ninjaaaa",
    "email": "rodrigo@mail.com",
    "login": "rodrigo",
    "admin": false
    },
    {
    "id": 334,
    "nome": "João Martinss",
    "email": "joão@mail.com",
    "login": "jonny",
    "admin": false
    },
    {
    "id": 336,
    "nome": "paulo",
    "email": "paulo@gmail.com",
    "login": "paulete",
    "admin": false
    },
    {
    "id": 399,
    "nome": "Alex",
    "email": "alex@gmail.com",
    "login": "alextton",
    "admin": false
    }
]
