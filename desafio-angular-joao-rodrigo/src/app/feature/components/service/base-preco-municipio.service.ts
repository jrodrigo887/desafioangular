import { HttpClient } from '@angular/common/http';
import { EventEmitter, Injectable } from '@angular/core';
import { BehaviorSubject, pipe, Subject } from 'rxjs';
import { Observable } from 'rxjs/internal/Observable';
import { map, switchMap } from 'rxjs/operators';
import { Municipio } from 'src/app/core/interfaces/municipioInterface';
import { environment } from 'src/environments/environment';

const api = `${environment.api_combustivel}/combustivel/media-de-preco`;
const api_valor_media = `${environment.api_combustivel}/combustivel/valor-media-compra-venda-municipio`;

@Injectable({
  providedIn: 'root'
})
export class BasePrecoMunicipioService {

  private _municipios$ = new BehaviorSubject<any>([]);


  constructor(private http: HttpClient) { }

  public getMediaPrecoMunicipio(municipio: string): Observable<any> {
    return this.http.get<any>(`${api}/${municipio}`);
  }

  public getMunicipios(): Observable<Municipio[]> {
    //retorna todos os municipios
    return this.http.get<Municipio[]>(api_valor_media);
  }

  get cidades() {
    return this._municipios$.getValue();
  }

  set cidades(value) {
    this._municipios$.next(value);
  }


}


