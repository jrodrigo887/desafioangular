import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { MetodosHttp } from 'src/app/core/enums/metodos.http';
import { HistoricoControle } from 'src/app/core/interfaces/historicoControle.modelo.interface';
import { environment } from 'src/environments/environment';

const api  = `${environment.api_combustivel}/historico`;

@Injectable({
  providedIn: 'root'
})
export class HistoricoService {

  constructor(private http: HttpClient) { }

  public getHistorico(): Observable<HistoricoControle[]> {
    return this.http.get<HistoricoControle[]>(api);
  }

  public getHistoricosById(id: number):
    Observable<HistoricoControle> {
    return this.http.get<HistoricoControle>(`${api}/${id}`);
  }

  public saveHistoricoPostOrPut(dados: HistoricoControle,
    metodo: MetodosHttp): Observable<any> {
    if (metodo === MetodosHttp.POST) {
      return this.http.post(api, dados);
    } else {
      return this.http.put(api, dados);
    }
  }

  public deleteHistorico(id: number): Observable<any> {
    return this.http.delete(`${api}/${id}`);
  }


}

