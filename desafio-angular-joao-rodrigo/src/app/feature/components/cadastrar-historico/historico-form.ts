import { AbstractControl, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { HistoricoControle } from 'src/app/core/interfaces/historicoControle.modelo.interface';

export class HistoricoForm extends FormGroup {
  private _errorMessages = {
    required: 'O campo %s é obrigatorio.',
    minLength: 'O campo não atingiu número mínimo de caracteres.',
    maxLength: 'O campo não atingiu número máximo de caracteres.',
  };

  constructor(){
    super({
      id: new FormControl(null),
      combustivel: new FormControl(null, [Validators.required]),
      data: new FormControl(null, [Validators.required]),
      preco: new FormControl(null, [Validators.required]),
    });
  }

  public get id(): AbstractControl {
    return this.get('id');
  }

  public get combustivel(): AbstractControl {
    return this.get('combustivel');
  }

  public get data(): AbstractControl {
    return this.get('data');
  }

  public get preco(): AbstractControl {
    return this.get('preco');
  }


  public getFirstErrorFrom(controlName: string, label: string): string {
    return this._errorMessages[Object.keys(this.get(controlName).errors)[0]].replace('%s', label || controlName);
  }

  public markAllAsTouched(): void {
    Object.keys(this.controls).map((control) => this.get(control).markAsDirty());
  }

  public setValues(data: HistoricoControle): void {
    this.id.setValue(data.id);
    this.combustivel.setValue(data.combustivel)
    this.data.setValue(data.data);
    this.preco.setValue(data.preco);
  }

  public getDadosEnvioHistorico(): HistoricoControle {
    return {
      id: 0,
      combustivel: this.combustivel.value,
      data: this.data.value,
      preco: this.preco.value,
    };
  }
}
