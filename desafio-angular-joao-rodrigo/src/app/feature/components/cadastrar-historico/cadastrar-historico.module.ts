import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CadastrarHistoricoComponent } from './cadastrar-historico.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import { ReactiveFormsModule } from '@angular/forms';
import {MatDialogModule} from '@angular/material/dialog';
import {MatInputModule} from '@angular/material/input';
import {MatTableModule} from '@angular/material/table';


@NgModule({
  declarations: [CadastrarHistoricoComponent],
  imports: [
    CommonModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule,
    MatDialogModule,
    MatTableModule,
  ],
})
export class CadastrarHistoricoModule { }
