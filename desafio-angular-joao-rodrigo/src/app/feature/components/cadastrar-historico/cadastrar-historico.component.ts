import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { HistoricoControle } from 'src/app/core/interfaces/historicoControle.modelo.interface';
import { DateUtil } from 'src/app/core/utils/date-util';
import { HistoricoForm } from './historico-form';


@Component({
  selector: 'app-cadastrar-historico',
  templateUrl: './cadastrar-historico.component.html',
  styleUrls: ['./cadastrar-historico.component.css']
})
export class CadastrarHistoricoComponent implements OnInit {
  histForm: FormGroup;


  constructor(
    private dialogRef: MatDialogRef<CadastrarHistoricoComponent>,
    @Inject(MAT_DIALOG_DATA) public dados: HistoricoControle) {
      console.log(dados);
      this.isEditar();
   }

  ngOnInit() {
  }

  private isEditar() {

    if ( !this.dados ) {

      this.histForm = new HistoricoForm();
    } else {

      this.histForm = new HistoricoForm();
      this.histForm.setValue({
        ...this.dados, data: DateUtil.formatDateBRforUS(this.dados.data)
      });
    }
  }

  public onSubmit() {
    const date = this.histForm.get('data').value;
    this.dialogRef.close({
      ...this.histForm.value,
    data: DateUtil.formatDateUSforBR(date)});
  }

  public onCancel() {
    this.dialogRef.close();
  }

}
