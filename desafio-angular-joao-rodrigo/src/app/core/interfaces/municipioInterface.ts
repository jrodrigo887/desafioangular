export interface Municipio  {
  atributo: string;
  mediaValorCompra?: number;
  mediaValorVenda?: number;
}
