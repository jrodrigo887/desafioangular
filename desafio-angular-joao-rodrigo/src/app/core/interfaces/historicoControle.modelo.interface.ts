
export interface HistoricoControle {

  combustivel: string;
  data: Date;
  id: number;
  preco: number;

}
