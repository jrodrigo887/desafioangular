export class DateUtil {
  static _formatoDefaul = Intl.NumberFormat('pt-br', {
    currency: 'BRL',
    style: 'currency'
  })

  static formatDateUSforBR(data) {
    const ano = data.split('-')[0];
    const mes = data.split('-')[1];
    const dia = data.split('-')[2];

    return `${dia}/${mes}/${ano}`;
  }

  static formatDateBRforUS(data) {
    const dia = data.split('/')[0];
    const mes = data.split('/')[1];
    const ano = data.split('/')[2];

    return `${ano}-${mes}-${dia}`;
  }

}
