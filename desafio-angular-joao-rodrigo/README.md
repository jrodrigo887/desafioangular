# DesafioAngularJoaoRodrigo

> **Lib utilizada**
* Angular Material

> **Desafio**

[ X ] Implementar recurso para CRUD de histórico de preço de combustível  
[ ] Implementar interface que retorne a média de preço de combustível com base no nome do município  
[ ] Implementar interface que retorne os dados agrupados por distribuidora  
[ ] Implementar interface que retorne o valor médio do valor da compra e do valor da venda por município  

__Observação__
  - Não foi possível concluir demais implementações, acima desmacada, devido ao meu tempo para conciliar no desenvolvimento, contudo estou disposto ainda, se caso possível, dêem mais tempo para poder continuar com a implementação.  
_______________
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.0.2.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
